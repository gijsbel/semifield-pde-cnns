import torch
import lietorch
from pde import *

"""
    Conv2D
    Batchnorm2D
    ReLU
"""
class CNN_Layer(torch.nn.Module):

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_radius: int,
    ):
        super().__init__()

        kernel_diameter = kernel_radius * 2 + 1

        self.model = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels, out_channels, kernel_diameter, bias=False, padding='same', padding_mode='replicate'),
            torch.nn.BatchNorm2d(out_channels, track_running_stats=False),
            torch.nn.ReLU(),
        )

        torch.nn.init.xavier_uniform_(
            self.model[0].weight, gain=torch.nn.init.calculate_gain('relu')
        )

    def forward(self, input):
        return self.model(input)
    

def PDE_Layer_Factory(pdes):
     
    class PDE_Layer(torch.nn.Module):
        def __init__(
            self,
            in_channels: int,
            out_channels: int,
            kernel_radius: int,
            mu: float = 1.0,
            alpha: float = 1.3,
            p: float = 2.0
        ):
            super().__init__()

            self.model = torch.nn.Sequential()

            self.model.append(lietorch.nn.ConvectionR2(in_channels))

            for pde in pdes:
                layer = None
                if pde=='Dif':
                    layer = QuadraticDiffusion(in_channels, kernel_radius)
                elif pde=='Dil':
                    layer = AlphaDilation(in_channels, kernel_radius, alpha)
                elif pde=='Ero':
                    layer = AlphaErosion(in_channels, kernel_radius, alpha)
                elif pde=='Log':
                    layer = QuadraticLogarithmic(in_channels, kernel_radius, mu)
                elif pde=='Roo':
                    layer = QuadraticRoot(in_channels, kernel_radius, p)
                else:
                    raise Exception(f"{pde} is not a valid pde identifier") 
                self.model.append(layer)

            self.model.append(torch.nn.Conv2d(in_channels, out_channels, 1, bias=False))
            self.model.append(torch.nn.BatchNorm2d(out_channels, track_running_stats=False))

        def forward(self, input):
            return self.model(input)
        
    return PDE_Layer