import torch
from helpers import morphological_kernel, quadratic_diffusion_kernel, infconv2d
    
class QuadraticDiffusion(torch.nn.Module):
    channels: int
    kernel_radius: int

    def __init__(
        self, 
        channels: int, 
        kernel_radius: int
    ):
        super().__init__()

        assert channels > 0, 'channels needs to be positive'
        assert kernel_radius >= 0, 'kernel_radius needs to be nonnegative'

        self.channels = channels
        self.kernel_radius = kernel_radius

        self.metric_params = torch.nn.Parameter(torch.Tensor(channels, 2, 2))

        self.reset_parameters()

    def reset_parameters(self):
        torch.nn.init.uniform_(self.metric_params, a=-1.0, b=1.0)

    def forward(self, input):
        assert self.channels == input.size(1), f'input has {input.size(1)} channels, should be {self.channels}'

        kernel = quadratic_diffusion_kernel(self.metric_params, self.kernel_radius)
        kernel = kernel.unsqueeze(1)
        k = self.kernel_radius
        padding = (k, k, k, k)
        x = torch.nn.functional.pad(input, padding, mode='reflect')
        x = torch.nn.functional.conv2d(x, kernel, padding=0, groups=self.channels)
        return  x

class QuadraticLogarithmic(torch.nn.Module):
    channels: int
    kernel_radius: int
    mu: float

    def __init__(
        self, 
        channels: int, 
        kernel_radius: int, 
        mu: float
    ):
        super().__init__()

        assert channels > 0, 'channels needs to be positive'
        assert kernel_radius >= 0, 'kernel_radius needs to be nonnegative'

        self.channels = channels
        self.kernel_radius = kernel_radius
        self.mu = mu

        self.diffusion = QuadraticDiffusion(channels,kernel_radius)

        self.reset_parameters()

    def reset_parameters(self):
        self.diffusion.reset_parameters()

    def forward(self, input):
        assert self.channels == input.size(1), f'input has {input.size(1)} channels, should be {self.channels}'

        x = torch.exp2(self.mu * input)
        x = self.diffusion(x)
        x = torch.log2(x) / self.mu
       
        return  x
    

class QuadraticRoot(torch.nn.Module):
    channels: int
    kernel_radius: int
    p: float

    def __init__(
        self, 
        channels: int, 
        kernel_radius: int, 
        p: float = 2.0
    ):
        super().__init__()

        assert channels > 0, 'channels needs to be positive'
        assert kernel_radius >= 0, 'kernel_radius needs to be nonnegative'

        self.channels = channels
        self.kernel_radius = kernel_radius
        self.p = p

        self.diffusion = QuadraticDiffusion(channels,kernel_radius)

        self.reset_parameters()

    def reset_parameters(self):
        self.diffusion.reset_parameters()

    def forward(self, input):
        assert self.channels == input.size(1), f'input has {input.size(1)} channels, should be {self.channels}'

        x = torch.abs(input)
        x = torch.pow(x + 1.0, self.p) - 1.0
        x = self.diffusion(x)
        x = torch.pow(x + 1.0, 1.0/self.p) - 1.0
       
        return  x
    
class AlphaErosion(torch.nn.Module):
    channels: int
    kernel_radius: int
    alpha: float

    def __init__(
        self, 
        channels: int, 
        kernel_radius: int,
        alpha: float
    ):
        super().__init__()

        assert channels > 0, 'channels needs to be positive'
        assert kernel_radius >= 0, 'kernel_radius needs to be nonnegative'
        assert alpha >= 1.0, 'alpha needs to be >= 1.0'

        self.channels = channels
        self.kernel_radius = kernel_radius
        self.alpha = alpha

        self.metric_params = torch.nn.Parameter(torch.Tensor(channels, 2, 2))

        self.reset_parameters()

    def reset_parameters(self):
        torch.nn.init.uniform_(self.metric_params, a=-1.0, b=1.0)

    def forward(self, input):
        assert self.channels == input.size(1), f'input has {input.size(1)} channels, should be {self.channels}'

        kernel = morphological_kernel(self.metric_params, self.kernel_radius, self.alpha)
        return infconv2d(input, kernel)
    
class AlphaDilation(torch.nn.Module):
    channels: int
    kernel_radius: int
    alpha: float

    def __init__(
        self, 
        channels: int, 
        kernel_radius: int, 
        alpha: float = 1.3
    ):
        super().__init__()

        assert channels > 0, 'channels needs to be positive'
        assert kernel_radius >= 0, 'kernel_radius needs to be nonnegative'
        assert alpha > 1.0, 'alpha needs to be > 1.0'

        self.channels = channels
        self.kernel_radius = kernel_radius
        self.alpha = alpha

        self.erosion = AlphaErosion(channels, kernel_radius, alpha)

        self.reset_parameters()

    def reset_parameters(self):
        self.erosion.reset_parameters()

    def forward(self, input):
        assert self.channels == input.size(1), f'input has {input.size(1)} channels, should be {self.channels}'

        return -self.erosion(-input)