import torch

# generic 6 layer CNN made from a given layers
class CNN(torch.nn.Module):
    def __init__(self, in_channels, channels, kernel_radius, layers, layer):
        super().__init__()

        c = channels
        k = kernel_radius

        self.model = torch.nn.Sequential()

        self.model.append(torch.nn.Conv2d(in_channels, c, 1, bias=False))
        for l in range(layers):
            self.model.append(layer(c, c, k))
        self.model.append(torch.nn.Conv2d(c, 1, 1, bias=False))
        self.model.append(torch.nn.Sigmoid())

    def forward(self, x):
        return self.model(x)