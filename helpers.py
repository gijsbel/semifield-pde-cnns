from pathlib import Path
import numpy as np
import torch
import math
from PIL import Image
import torchvision.transforms as transforms 

def get_datasets(data_dir, patch_shape=None, patch_overlap=None):
    '''
        Load and pre-process DRIVE images (x) and segmentation maps (y).
    '''
    path_x = Path(data_dir) / 'training' / 'images'
    path_y = Path(data_dir) / 'training' / '1st_manual'

    x = []
    y = []

    pil_to_tensor = transforms.PILToTensor() 

    for p in sorted(path_x.glob('*.png')):
        image = Image.open(p)
        image = pil_to_tensor(image).float() / 255.0
        x.append(image)

    for p in sorted(path_y.glob('*.gif')):
        image = Image.open(p)
        image = pil_to_tensor(image).float() / 255.0
        y.append(image)

    x = torch.stack(x) # [B, C, H, W]
    y = torch.stack(y) # [B, C, H, W]

    B, C, H, W = x.shape

    # Split training data into patches if requested
    if patch_shape != None:
        # first calculate anchor (corner) point for the individual patches
        
        patch_h, patch_w = patch_shape
        overlap_h, overlap_w = patch_overlap

        anchors_h = list(range(0, H, patch_h - overlap_h))

        if anchors_h[-1] >= H - overlap_h:
            anchors_h = anchors_h[:-1]
        anchors_h[-1] = H - patch_h

        anchors_w = list(range(0, W, patch_w - overlap_w))

        if anchors_w[-1] >= W - overlap_w:
            anchors_w = anchors_w[:-1]
        anchors_w[-1] = W - patch_w

        anchors = np.reshape(
            np.stack(np.meshgrid(anchors_h, anchors_w), axis=-1), (-1, 2)
        )

        x_patches = []
        y_patches = []

        for i in range(x.shape[0]):
            for j, a in enumerate(anchors):
                x_patch = x[i, :, a[0] : a[0] + patch_h, a[1] : a[1] + patch_w]
                y_patch = y[i, :, a[0] : a[0] + patch_h, a[1] : a[1] + patch_w]
                # if (y_patch == 0.0).all():
                #    continue
                if (y_patch==0).all():
                    # print(f"patch {i},{j} omitted: no segmentation")
                    continue
                x_patches.append(x_patch)
                y_patches.append(y_patch)

        x = torch.stack(x_patches)
        y = torch.stack(y_patches)

    train_set = torch.utils.data.TensorDataset(x, y)

    path_x_test = Path(data_dir) / 'test' / 'images'
    path_y_test = Path(data_dir) / 'test' / '1st_manual'

    x_test = []
    y_test = []

    for p in sorted(path_x_test.glob('*.png')):
        image = Image.open(p)
        image = pil_to_tensor(image).float() / 255.0
        x_test.append(image)

    for p in sorted(path_y_test.glob('*.gif')):
        image = Image.open(p)
        image = pil_to_tensor(image).float() / 255.0
        y_test.append(image)

    x_test = torch.stack(x_test)
    y_test = torch.stack(y_test)

    test_set = torch.utils.data.TensorDataset(x_test, y_test)

    return train_set, test_set

def norm_squared_kernel(metric_params: torch.Tensor, kernel_radius: int) -> torch.Tensor:
    
    c = metric_params.size(dim=0)
    k = kernel_radius
    d = 2 * k + 1
    r = torch.arange(-k, k+1).to(metric_params.device)
    y, x = torch.meshgrid(r, r, indexing='ij')

    v = torch.stack((x,y),dim=0)
    v = v.float()
    v = v.reshape(2, d*d)
          
    Hv = metric_params @ v

    norm2 = torch.sum(Hv.square(),dim=1)
    norm2 = norm2.reshape(c,d,d)

    # equivalent way using einsum:
    # G = torch.transpose(H,1,2) @ H
    # norm2 = torch.einsum('cij,xyi,xyj->cxy',G,v,v)

    return norm2

def morphological_kernel(metric_params: torch.Tensor, kernel_radius: int, alpha: float) -> torch.Tensor:
    # 1/alpha + 1/beta = 1
    beta = 1.0/(1.0 - 1.0/alpha)
    norm2 = norm_squared_kernel(metric_params,kernel_radius)
    kernel = 1.0 / beta * norm2.pow(beta / 2.0)
    return kernel

def quadratic_diffusion_kernel(metric_params: torch.Tensor, kernel_radius: int) -> torch.Tensor:
    norm2 = norm_squared_kernel(metric_params,kernel_radius)
    kernel = torch.exp(-norm2)
    kernel = kernel / kernel.sum(dim=(1,2))[:,None,None]
    return kernel

# https://github.com/adrien-castella/PDE-based-CNNs/blob/main/modules.py
def infconv2d(input: torch.Tensor, kernel: torch.Tensor) -> torch.Tensor:
    assert input.size(1) == kernel.size(0)
    
    B, C, H, W = input.size()
    C, kH, kW = kernel.size()
    
    padding = (math.floor((kH-1)/2), math.ceil((kH-1)/2), math.floor((kW-1)/2), math.ceil((kW-1)/2))
    x = torch.nn.functional.pad(input, padding, mode='constant', value=torch.finfo().max)
    x = torch.nn.functional.unfold(x, (kH, kW)) # [B, C*kH*kW, H*W] 

    kernel = kernel.view(-1) # [C*kH*kW]
    kernel = kernel.unsqueeze(0).unsqueeze(-1) # [1, C*kH*kW, 1]

    # x and kernel now have shapes compatible for broadcasting:
    x = x + kernel  # [B, C*kH*kW, H*W]
    x = x.view(B, C, kH*kW, x.size(-1))  # [B, C, kH*kW, H*W]

    # taking the min over block elements
    x, _ = torch.min(x, dim=2) # [B, C, H*W]
    x = x.view(B, C, H, W) # [B, C, H, W]

    return x
