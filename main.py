import torch
import lietorch
import wandb
import numpy as np

from helpers import get_datasets
from layer import *
from model import CNN

print(f'LieTorch version: {lietorch.__version__}')
print(f'LieTorch location: {lietorch.__file__}')
print(f'PyTorch version: {torch.__version__}')
print(f'PyTorch location: {torch.__file__}')

def dice(output, target, epsilon=1.0):
    AinterB = (output * target).sum()
    A = output.sum()
    B = target.sum()
    dice = (2 * AinterB + epsilon) / (A + B + epsilon)
    return dice

def loss(output, target):
    return 1.0 - dice(output, target)

# train on a single batch and return batch loss
def train(model, device, optimizer, batch):
    model.train()

    x, y = batch

    x, y = x.to(device), y.to(device)

    optimizer.zero_grad()
    output = model(x)

    batch_loss = loss(output, y)

    batch_loss.backward()
    optimizer.step()

    return batch_loss.item()

def test(model, device, test_loader):
    model.eval()

    test_batch_dices = []

    with torch.no_grad():
        for batch_id, (x, y) in enumerate(test_loader):
            x, y = x.to(device), y.to(device)

            output = model(x)

            prediction = output.round()
            
            batch_dice = dice(prediction, y, epsilon=1.0).item()
            test_batch_dices.append(batch_dice)

    test_dice = np.mean(test_batch_dices)

    return test_dice

PDES_OPTIONS = [
    ['Dif'],
    ['Dil'],
    ['Ero'],
    ['Log'],
    ['Roo'],

    ['Log','Roo'],
    ['Ero','Roo'],
    ['Ero','Log'],
    ['Dil','Roo'],
    ['Dil','Log'],
    ['Dil','Ero'],

    ['Ero','Log','Roo'],
    ['Dil','Log','Roo'],
    ['Dil','Ero','Roo'],
    ['Dil','Ero','Log'],

    ['Dil','Ero','Log','Roo'],
]

for PDES in PDES_OPTIONS:

    for RUN in range(5):

        LAYER = PDE_Layer_Factory(PDES)
        IN_CHANNELS = 3 
        LAYERS = 6
        CHANNELS = 25
        KERNEL_RADIUS = 4
        BATCHES = 20000
        DATA_PATH = 'S:\Lieanalysis\VICI\datasets\DRIVE_png'
        TRAIN_SET_RATIO = 1.0
        PATCH_SHAPE = [64, 64] # smaller is worse
        PATCH_OVERLAP = [16, 16]
        TRAIN_BATCH_SIZE = 8 # 8 is fine, higher is worse
        TEST_BATCH_SIZE = 1 # keep 1 for the batchnorm and correct test loss!, higher is worse
        LR = 0.01 # 0.1 is way too high, should not go smaller than 0.001
        WEIGHT_DECAY = 0.01 # 0.001 does not make it better
        SCHEDULER_END_FACTOR = 0.1
        SCHEDULER_ITERS = 1000
        WANDB = True

        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        cc = torch.cuda.get_device_capability(device)

        train_set, test_set = get_datasets(DATA_PATH, PATCH_SHAPE, PATCH_OVERLAP)
        train_set = torch.utils.data.random_split(train_set, [TRAIN_SET_RATIO, 1.0 - TRAIN_SET_RATIO])[0]
        
        train_loader = torch.utils.data.DataLoader(train_set, batch_size=TRAIN_BATCH_SIZE, shuffle=True)
        train_loader_iterator = iter(train_loader)
        test_loader = torch.utils.data.DataLoader(test_set, batch_size=TEST_BATCH_SIZE, shuffle=False)

        model = CNN(IN_CHANNELS,CHANNELS,KERNEL_RADIUS,LAYERS,LAYER).to(device)
        optimizer = torch.optim.AdamW(model.parameters(), lr=LR, weight_decay=WEIGHT_DECAY)
        scheduler = torch.optim.lr_scheduler.LinearLR(optimizer, start_factor=1.0, end_factor=SCHEDULER_END_FACTOR, total_iters=SCHEDULER_ITERS)

        total_params = sum(p.numel() for p in model.parameters(recurse=True))

        config={
            'device': device,
            'device name': torch.cuda.get_device_name(device),
            'compute capabilities': cc,
            'pdes': PDES,
            'layer class name': LAYER.__name__,
            'in channels': IN_CHANNELS,
            'hidden channels': CHANNELS,
            'kernel radius': KERNEL_RADIUS,
            'total parameter': total_params,
            'dataset path': DATA_PATH,
            'train set ratio': TRAIN_SET_RATIO,
            'train set size': len(train_set),
            'patch shape': PATCH_SHAPE,
            'patch overlap': PATCH_OVERLAP,
            'train batch size': TRAIN_BATCH_SIZE,
            'test batch size': TEST_BATCH_SIZE,
            'batches': BATCHES,
            'starting learning rate': LR,
            'scheduler end factor': SCHEDULER_END_FACTOR,
            'scheduler iterations': SCHEDULER_ITERS,
            'wandb': WANDB,
            'run': RUN
        }

        for key, value in config.items():
            print(f'{key}: {value}')
        
        if WANDB:
            wandb.init(project='DRIVE segmentation',config=config)

        for batch_number in range(1, BATCHES + 1):
            try:
                batch = next(train_loader_iterator)
            except StopIteration:
                train_loader_iterator = iter(train_loader)
                batch = next(train_loader_iterator)

            batch_loss = train(model, device, optimizer, batch)

            print(f'Train Batch: {batch_number}/{BATCHES} Train Batch Loss: {batch_loss:.5f} LR: {scheduler.get_last_lr()[0]:.5f}',  end='\r')
            if WANDB:
                wandb.log({
                    'batch number': batch_number,
                    'batch loss': batch_loss,
                    'lr': scheduler.get_last_lr()[0],
                })

            if batch_number % 200 == 0:
                test_dice = test(model, device, test_loader)
                print('')
                print(f'Test dice: {test_dice:.5f}')
                if WANDB:
                    wandb.log({
                        'test dice': test_dice,
                    })
                
            scheduler.step()

        if WANDB:
            wandb.finish()