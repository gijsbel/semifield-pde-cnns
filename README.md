
## Installation

The preferred installation method uses [Anacondo](https://docs.anaconda.com/free/miniconda/index.html).

Create and activate a new or existing environment
```shell
  conda create -n <environment name>
  conda activate <environment name>
```
Install [PyTorch](https://pytorch.org/get-started/locally/), [LieTorch](https://gitlab.com/bsmetsjr/lietorch/), [Numpy](https://numpy.org/), [Weights & Biases](https://docs.wandb.ai/) 
```shell
  conda install pytorch==2.0.1 torchvision==0.15.2 torchaudio==2.0.2 pytorch-cuda=11.8 -c pytorch -c nvidia
  conda install -c lietorch lietorch
  conda install numpy
  pip install wandb
```
Optionally install [Matplotlib](https://matplotlib.org/) to run ```test.ipynb```
```shell
    conda install matplotlib
```
Make sure your [Nvidia drivers](https://www.nvidia.com/download/index.aspx) are up to date!
